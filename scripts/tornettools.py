""" This file contains code taken directly from https://github.com/shadow/tornettools/blob/main/tornettools/generate.py

    See the LICENSE file for the licensing information
"""

import random
import lzma
import networkx as nx
from ipaddress import IPv4Address

def parse_network(model_path):
    with lzma.open(model_path) as f:
        print("Reading compressed network graph {}".format(model_path))
        network = nx.readwrite.gml.read_gml(f, label='id')
        print("Finished reading network graph")
        return network


def find_node(network, node):
    ip_address_hint = IPv4Address(node['address']) if 'address' in node else None
    country_code_hint = node.get('country_code')
    chosen_node = random.choice(filter_nodes(network, ip_address_hint, country_code_hint))
    return chosen_node

def filter_nodes(network, ip_address_hint, country_code_hint):
    # networkx stores the node 'id' separately, so take the node id from the tuple and combine it
    # with the other node properties
    all_nodes = [{'id': node_id, **node} for (node_id, node) in network.nodes(data=True)]

    if ip_address_hint is not None and not ip_address_hint.is_global:
        # ignore the hint if the IP address is not global
        logging.debug(f"Ignoring non-global address {ip_address_hint}")
        ip_address_hint = None

    if country_code_hint is not None:
        # normalize the country code
        country_code_hint = country_code_hint.casefold()

    # are there any nodes with the same ip address?
    ip_match_found = any('ip_address' in node and IPv4Address(node['ip_address']) == ip_address_hint for node in all_nodes)

    if ip_match_found:
        # get all nodes with exact IP matches, regardless of the country code
        candidate_nodes = [node for node in all_nodes if 'ip_address' in node and IPv4Address(node['ip_address']) == ip_address_hint]
    else:
        # get all nodes with the same country code
        candidate_nodes = [node for node in all_nodes if 'country_code' in node and node['country_code'].casefold() == country_code_hint]

        # if no node had the same country code, use all nodes
        if len(candidate_nodes) == 0:
            candidate_nodes = [node for node in all_nodes]

        any_ip_found = any('ip_address' in node for node in candidate_nodes)

        # if a node has an IP address and we were given an IP hint, perform longest prefix matching
        if any_ip_found and ip_address_hint is not None:
            # exclude nodes without an IP address
            candidate_nodes = [node for node in candidate_nodes if 'ip_address' in node]

            # function to compute the prefix match between two IPv4 addresses
            # the 32-bit mask is required since python uses signed integers
            #   (see https://stackoverflow.com/questions/210629/python-unsigned-32-bit-bitwise-arithmetic/210740)
            def compute_prefix_match(ip_1, ip_2): return ~(int(ip_1)^int(ip_2)) & 0xffffffff

            # get the prefix match for each node
            prefix_matches = [(node, compute_prefix_match(IPv4Address(node['ip_address']), ip_address_hint)) for node in candidate_nodes]

            # get the longest prefix match
            max_prefix_match = max(prefix_matches, key=lambda x: x[1])[1]

            # get the nodes with the longest prefix match
            candidate_nodes = [node for (node, prefix_match) in prefix_matches if prefix_match == max_prefix_match]

            # given the 'compute_prefix_match' function above, these nodes should all have the same IP address
            assert len(set([IPv4Address(node['ip_address']) for node in candidate_nodes])) == 1

    return candidate_nodes
