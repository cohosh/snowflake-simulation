#!/usr/bin/env python3

import os
import yaml
import argparse
import random
import re
from numpy.random import choice
from datetime import datetime
from statistics import median

import tornettools

# for parsing snowflake stats
date_re = re.compile(r'snowflake-stats-end (\d{4}-\d\d-\d\d)')
proxy_counts_re = re.compile(r'^snowflake-ips (\w\w=[\d\?]+,)*\w\w=[\d\?]+')

# Calculates the probability for which proxies should be sampled from each
# country code using snowflake statistics from CollecTor:
# https://metrics.torproject.org/collector.html#snowflake-stats
# TODO: explain that we take the median over a months' worth of stats
def get_proxy_probs(sf_stats_path):
    print("Parsing stats")
    stats = parse_sf_stats(sf_stats_path)
    print("Finished parsing stats")

    probs_by_cc = {}
    for time in stats:
        total_proxies = float(sum(stats[time].values()))
        for cc in stats[time]:
            prob = stats[time][cc] / total_proxies
            probs_by_cc.setdefault(cc, []).append(prob)

    output = {}
    for cc in probs_by_cc:
        probs = probs_by_cc[cc]
        med = median(probs) if len(probs) > 0 else 0.0
        output.setdefault(cc, med)

    # normalize
    total_prob = float(sum(output.values()))
    for cc in output:
        output[cc] = output[cc]/total_prob

    return output

def parse_sf_stats(sf_stats_path):

    counts_by_time = {}
    for root, _, filenames in os.walk(sf_stats_path):
        for filename in filenames:
            with open (os.path.join(root, filename), 'r') as f:
                unix_time = 0
                for line in f:
                    if date_re.match(line):
                        date = line.strip().split(' ')[1]
                        dt = datetime.strptime(date, "%Y-%m-%d")
                        unix_time = int(dt.strftime("%s"))
                        counts_by_time.setdefault(unix_time, {})
                    if proxy_counts_re.match(line):
                        counts = line.lstrip("snowflake-ips ").split(',')
                        for entry in counts:
                            vals = entry.split("=")
                            counts_by_time[unix_time].setdefault(vals[0], int(vals[1]))
    return counts_by_time

def get_proxies(num, proxy_probs):
    proxies = []

    # Simulate a snowflake network with 100 proxies
    for i in range(0, num):
        # sample each proxy location from probability distribution of country codes
        location = choice(sorted(proxy_probs.keys()), p=[float(proxy_probs[country]) for country in proxy_probs.keys()] )
        proxy = {
                'name': 'proxy{}'.format(i+1),
                'country_code': location,
        }
        proxies.append(proxy)
    return proxies


def sf_clients(num, network, country, binary_path):
    # filter the network graph nodes by their country, and choose one node
    country_code_hint = country
    chosen_node = random.choice(tornettools.filter_nodes(network, None, country_code_hint))

    hosts = {}
    for i in range(0, num):
        # add the host element and attributes
        host = {}
        host['network_node_id'] = chosen_node['id']

        # Note: these bw numbers are from speedtest.net for the global average mobile user
        host["bandwidth_up"] = "{} Mbit".format(13)
        host["bandwidth_down"] = "{} Mbit".format(57)

        host["processes"] = []

        process = {}
        process["path"] = "{}/snowflake-client".format(binary_path)
        process["args"] = '-ice "stun:stun:3478" -url "http://broker:8080" -keep-local-addresses -log "pt.log" -unsafe-logging'
        env = {}
        env["TOR_PT_MANAGED_TRANSPORT_VER"] = "1"
        env["TOR_PT_CLIENT_TRANSPORTS"] = "snowflake"
        process["environment"] = env
        process["start_time"] = 90 # start after proxy has had time to poll
        process["expected_final_state"] = "running"

        host["processes"].append(process)

        process = {}
        process["path"] = "{}/tgen".format(binary_path)
        process["args"] = "../../../../conf/tgen.client.graphml.xml"
        # tgen starts at the end of shadow's "bootstrap" phase, and may have its own startup delay
        process["start_time"] = 100
        process["expected_final_state"] = "running"

        host["processes"].append(process)
        hosts.update({"client{}".format(i): host})
    return hosts

def sf_server(network, country, address, binary_path):
    node = {
        "address": address,
        "country": country,
    }
    chosen_node = tornettools.find_node(network, node)

    # add the host element and attributes
    host = {}
    host['network_node_id'] = chosen_node['id']

    # Note: add based on our plan
    host["bandwidth_up"] = "1 Gbit"
    host["bandwidth_down"] = "1 Gbit"

    host["processes"] = []

    process = {}
    process["path"] = "{}/snowflake-server".format(binary_path)
    process["args"] = '-disable-tls'
    env = {}
    env["TOR_PT_MANAGED_TRANSPORT_VER"] = "1"
    env["TOR_PT_SERVER_BINDADDR"] = "snowflake-0.0.0.0:8000"
    env["TOR_PT_SERVER_TRANSPORTS"] = "snowflake"
    env["TOR_PT_ORPORT"] = "127.0.0.1:8080"
    process["environment"] = env
    process["start_time"] = 90 # start after proxy has had time to poll
    process["expected_final_state"] = "running"

    host["processes"].append(process)

    process = {}
    process["path"] = "{}/tgen".format(binary_path)
    process["args"] = "../../../../conf/tgen.server.graphml.xml"
    process["start_time"] = 1
    process["expected_final_state"] = "running"

    host["processes"].append(process)

    return {"server": host}

def sf_proxy(network, proxy_info, binary_path):
    # filter the network graph nodes by their country, and choose one node
    country_code_hint = proxy_info['country_code']
    chosen_node = random.choice(tornettools.filter_nodes(network, None, country_code_hint))

    # add the host element and attributes
    host = {}
    host['network_node_id'] = chosen_node['id']

    # Note: these bw numbers are from speedtest.net for the global average desktop user
    host["bandwidth_up"] = "{} Mbit".format(60)
    host["bandwidth_down"] = "{} Mbit".format(110)


    process = {}
    process["path"] = "{}/snowflake-proxy".format(binary_path)
    process["args"] = '-keep-local-addresses -broker "http://broker:8080" -relay "ws://server:8000" -stun "stun:stun:3478" -allowed-relay-hostname-pattern ^server$ -allow-non-tls-relay'
    process["start_time"] = 30 # start proxies after snowflake infra has had time to setup
    process["expected_final_state"] = "running"

    host["processes"] = []
    host["processes"].append(process)

    return {proxy_info["name"]: host}


def sf_infra(network, node, name, path, args):
    chosen_node = tornettools.find_node(network, node)

    host = {}
    host['network_node_id'] = chosen_node['id']

    if 'address' in node:
        host["ip_addr"] = node['address']

    host["bandwidth_down"] = "1 Gbit"
    host["bandwidth_up"] = "1 Gbit"

    process = {}
    process["path"] = path
    process["args"] = args
    process["start_time"] = 1
    process["expected_final_state"] = "running"

    host['processes'] = []
    host['processes'].append(process)

    return {name: host}

def network_config(model_path):
    network = {}
    network["use_shortest_path"] = False
    network["graph"] = {}
    network["graph"]["type"] = "gml"
    network["graph"]["file"] = {}
    network["graph"]["file"]["path"] = model_path
    network["graph"]["file"]["compression"] = "xz"
    return {"network": network}

def main():
    parser = argparse.ArgumentParser(
            description="Generate a shadow config file for Snowflake simulations in Shadow")
    parser.add_argument('--trials', type=int, help="Number of simulations to generate",
                        default=1)
    parser.add_argument('--clients', type=int, help="Number of clients to include in the simulation",
                        default=1)
    parser.add_argument('--proxies', type=int, help="Number of Snowflake proxies to simulate",
                        default=100)
    parser.add_argument('--binary-path', type=str,
                        help="Path to a directory containing snowflake binaries",
                        default="~/.local/bin")
    parser.add_argument('--model-path', type=str, help="Path to the compressed atlas network model",
                        required=True)
    parser.add_argument('--snowflake-metrics-path', type=str,
                        help="Path to a directory containing one month of snowflake broker metrics",
                        required=True)
    parser.add_argument('--simulation-dir-prefix', type=str,
                        help="Directory prefix where simulation config files will be written",
                        required=True)
    args = parser.parse_args()

    binary_path = os.path.abspath(os.path.expanduser(args.binary_path))
    model_path = os.path.abspath(os.path.expanduser(args.model_path))

    network = tornettools.parse_network(model_path)

    for i in range(0, args.trials):

        config = {}
        config["general"] = {}
        config["general"]["stop_time"] = "30 min"

        config.update(network_config(model_path))

        config["hosts"] = {}
        # Snowflake broker
        node = {
            "address": "37.218.245.111",
            "name": "broker",
            "country": "NL",
        }
        config["hosts"].update(sf_infra(network, node, "broker",
            "{}/snowflake-broker".format(binary_path),
            '-addr ":8080" -disable-tls -unsafe-logging -allowed-relay-pattern ^server$ -bridge-list-path ../../../../conf/bridge-list.json'))

        # STUN server
        node = {
            "name": "stun",
            "country": "US",
        }
        config["hosts"].update(sf_infra(network, node, "stun",
            "{}/stund".format(binary_path),
            ""))

        config["hosts"].update(sf_clients(args.clients, network, "CA", binary_path))
        config["hosts"].update(sf_server(network, "NL", "37.218.242.151", binary_path))

        proxy_probs = get_proxy_probs(args.snowflake_metrics_path)
        proxies = get_proxies(args.proxies, proxy_probs)
        for proxy in proxies:
            config["hosts"].update(sf_proxy(network, proxy, binary_path))

        os.mkdir("{}-{}".format(args.simulation_dir_prefix, i))
        with open("{}-{}/shadow.config.yaml".format(args.simulation_dir_prefix, i), 'w') as configfile:
            yaml.dump(config, configfile, sort_keys=False)

if __name__ == '__main__':
    main()
