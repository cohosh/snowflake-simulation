Shadow simulation tools for Snowflake. The scripts are inspired by [tornettools](https://github.com/shadow/tornettools).

## Prerequisites

We'll need to compile and prepare Snowflake for shadow experimentation. Also make sure that tgen and shadow are installed in `~/.local/bin` as per instructions in the [shadow user manual](https://shadow.github.io/docs/guide/getting_started_tgen.html).

### Preparing Snowflake binaries

These scripts assume the following Snowflake binaries are installed in a single directory (such as `~/.local/bin`):
- snowflake-server
- snowflake-broker
- snowflake-proxy
- snowflake-client

Snowflake requires a patch to work correctly in a closed environment. A working [branch with this patch](https://gitlab.torproject.org/cohosh/snowflake/-/tree/shadow) is a available, the last commit of which may be rebased ontop of a more recent version of Snowflake. To compile and install the above binaries: clone the repo, check out the shadow branch, and run the following commands.

```
cd server
go build
cp server ~/.local/bin/snowflake-server

cd ../broker
go build
cp broker ~/.local/bin/snowflake-broker

cd ../proxy
go build
cp proxy ~/.local/bin/snowflake-proxy

cd ../client
go build
cp client ~/.local/bin/snowflake-client
```

### Preparing STUN binary

You also need a binary for the STUN server. The simplest one I have found that currently works in Shadow is a Go project called [stund](https://github.com/gortc/stund). To install in the right place, simply run

`GOBIN=~/.local/bin go install github.com/gortc/stund@latest`

### Download data

You will need to download the following data to construct the network graph and sample the Snowflake proxy network:

- [Compressed atlas network model](https://github.com/tmodel-ccs2018/tmodel-ccs2018.github.io/blob/master/data/shadow/network/atlas_v201801.shadow_v2.gml.xz)

   ```
   git clone https://github.com/tmodel-ccs2018/tmodel-ccs2018.github.io.git
   ```

- Snowflake statistics

  We use statistics from CollecTor to sample proxies from locations representative of currently deployed Snowflakes. 

  ```
  wget https://collector.torproject.org/archive/snowflakes/snowflakes-2023-07.tar.xz
  tar -xvf snowflakes-2023-07.tar.xz
  ```

## Running a single simulation

First generate the config file in the specified simulation directory:
```
./scripts/generate_yaml.py --model-path tmodel-ccs2018.github.io/data/shadow/network/atlas_v201801.shadow_v2.gml.xz --snowflake-metrics-path snowflakes-2023-07/ --simulation-dir-prefix simulation
```

Then, change into the simulation directory and run:
```
cd simulation-0/
shadow --model-unblocked-syscall-latency=true shadow.config.yaml > shadow.log
```

